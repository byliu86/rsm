#!/bin/ksh
#PJM -L "node=21:noncont"
#PJM -L elapse=06:00:00
#PJM -L node-mem=unlimited
#PJM --no-stging
#PJM --mpi "proc=324"
#PJM -j

set -x

machine=fx10
export RMPI=324 
OMP=1
source /users/xa09/sample/setup_mpi+omp.${machine} $OMP

export PWDIR=`cd ../../; pwd`
echo $PWDIR

RSMPATH=${PWDIR}/run; cd ${RSMPATH} || exit 8
RSM_FCST=${RSMPATH}/exe/rsm.x
. ${RSMPATH}/run.sh ${PWDIR} ||  exit 8

#
#RSM
#/usr/bin/time -p mpiexec  -stdin rfcstparm.all --of-proc stdout.mpmd -n $RMPI ${RSM_FCST}
/usr/bin/time -p mpiexec  -stdin rfcstparm.all -n $RMPI ${RSM_FCST}
if [ $? != 0 ] ; then
  echo "error occured: fct model fail !!"
fi

