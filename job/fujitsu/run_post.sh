#!/bin/ksh
#PJM -L "node=1:noncont"
#PJM -L elapse=01:30:00
#PJM -L node-mem=unlimited
#PJM --no-stging
#PJM --mpi "proc=1"
#PJM -j

#set -x

machine=fx10
OMP=1
source /users/xa09/sample/setup_mpi+omp.${machine} $OMP

export PWDIR=`cd ../../; pwd`
echo $PWDIR
RSMPATH=${PWDIR}/run; cd ${RSMPATH} || exit 8
. ${RSMPATH}/configure ||  exit 8
echo $SHSDIR  
echo $RUNDIR

# cd to the RSM output directory
cd $RUNDIR

# run RSM post processor
for ihr in {0..${FCSTHR}..${PRTHOUR}};
do
  echo "processing fhour: $ihr"
  /usr/bin/time ${SHSDIR}/run_rpgb.sh  $ihr
  wait
done
echo "Finish RSM rpgb"
