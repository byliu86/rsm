dset ^r4d1slmk_grib2.bin
options sequential big_endian
undef -9.99E+33
title check
xdef  92 linear  60.00  0.25
ydef  77 linear -20.0   0.25
zdef   47 linear 1 1
tdef 1 linear 12Z13OCT2018         1hr
vars 1
 terr 0 99  fm2x
endvars
