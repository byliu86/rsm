      Program domain1
      implicit none

      real xlon1,xlon2,ylat1,ylat2,rgrdsz
      parameter(xlon1=115.08,xlon2=126.00,ylat1=18.96,ylat2=28.08,     &
                rgrdsz=0.12)
      integer nx1,ny1,nx2,ny2
!      parameter(nx1=1440,ny1=721)
      parameter(nx1=3000,ny1=1501)
!      parameter(nx2=480 ,ny2=361)
      parameter(nx2=nint((xlon2-xlon1)/rgrdsz+1.))
      parameter(ny2=nint((ylat2-ylat1)/rgrdsz+1.))
      real*8,dimension(nx1,ny1)::  aa
      real*8,dimension(nx2,ny2)::  bb


      integer i,j,ii,jj
      integer x1,x2,y1,y2
      x1=nint((xlon1-0.)/rgrdsz+1.)
      x2=nint((xlon2-0.)/rgrdsz+1.)
      y1=nint((ylat1-(-90.))/rgrdsz+1.)
      y2=nint((ylat2-(-90.))/rgrdsz+1.)
      print*,(x2-x1+1), nx2
      print*,(y2-y1+1), ny2

      if ((x2-x1+1).ne. nx2) stop 'dimension error in lon'
      if ((y2-y1+1).ne. ny2) stop 'dimension error in lat'

! ../terr_grib2.bin
! ../slmk_grib2.bin
      open(11,file='../to_3000_1501.bin',form='unformatted',           &
          convert='big_endian',status='old')
      open(12,file='d1terr_grib2.bin',form='unformatted',              &
          convert='big_endian',status='unknown')
      open(21,file='../ls_3000_1501.bin',form='unformatted',           &
          convert='big_endian',status='old')
      open(22,file='d1slmk_grib2.bin',form='unformatted',              &
          convert='big_endian',status='unknown')
      ! they are big_endian
!terr
      read(11) aa

      jj=0
      do j=y1,y2
        jj=jj+1
        ii=0
        do i=x1,x2
          ii=ii+1
          bb(ii,jj)=aa(i,j)
          if (ii.gt.nx2) stop 'dimension x error'
        enddo
          if (jj.gt.ny2) stop 'dimension y error'
      enddo

      write(12) bb

      close(11)
      close(12)
!slmk
      read(21) aa

      jj=0
      do j=y1,y2
        jj=jj+1
        ii=0
        do i=x1,x2
          ii=ii+1
          bb(ii,jj)=aa(i,j)
          if (ii.gt.nx2) stop 'dimension x error'
        enddo
          if (jj.gt.ny2) stop 'dimension y error'
      enddo

      write(22) bb

      close(21)
      close(22)
!
      stop 0
      endprogram

!=======
! compile: ifort domain1.f90/ gfortran domain1.f90
! compile: "frtpx domain1.f90" then "pjsub a.out"

