# build library and some executable files

#
# at_MACHINE : fx10, fx100, pcc
#
if [ $# == 2 ] ; then
  export at_MACHINE=${1}
  export do_testcase=${2}
else
  echo "usage: $0 [at_MACHINE] [do_testcase]"
  echo "  at_MACHINE can be fx10, fx100, or pcc"
  echo "  do_testcase equals yes will compile a test case; otherwise, go to run/configure to setup your own case"
  echo "  FOR EXAMPLE: ./build.sh fx10 yes"
  exit
fi

export PWDPATH=` pwd `

# compile options for pcc:linux_intel, fx10:Fujitsu_fx10, fx100:Fujitsu_fx100
if [ ${at_MACHINE} = pcc ] ; then
  export MACHINE=linux_intel
elif [ ${at_MACHINE} = fx10 ] ; then
  export MACHINE=Fujitsu_fx10
elif [ ${at_MACHINE} = fx100 ] ; then
  export MACHINE=Fujitsu_fx100
else
  echo "can't support your machine"
  exit 88
fi

#---------------------lib--------------------------
export LIBPATH=`cd ./lib ; pwd `

if [ ${MACHINE} = linux_intel ] ; then
export LINKPATH=/home/xb127/pkg
elif [ ${MACHINE} = Fujitsu_fx10 ] ; then
export LINKPATH=/nwpr/gfs/xb127/pkg/fx10
elif [ ${MACHINE} = Fujitsu_fx100 ] ; then
export LINKPATH=/nwpr/gfs/xb127/pkg/fx100
else
echo "can't support your machine"
echo "please define your path for NCEPLIB and netcdf library"
echo " export LINKPATH=..."
exit 88
fi

if [ ! -d "${LINKPATH}" ];then echo "package path doesn't exist, please build the pkg";exit 88;fi

#clean all old lib and unlink every file in 
rm -rf ${LIBPATH}/*.a ${LIBPATH}/*.la ${LIBPATH}/incmod
find -maxdepth 1 -type l -delete

#build lib comes with RSM
#cd ${LIBPATH}/src || exit 81
#. compile.sh  || exit 82

#link library to LIBPATH
cd ${LIBPATH} || exit 81
[ -d "${LINKPATH}/jasper-1.900.1" ] && ln -fs ${LINKPATH}/jasper-1.900.1 . || echo "${LINKPATH}/jasper-1.900.1 not exist"
if [ ${MACHINE} = linux_intel ] ; then
  [ -d "${LINKPATH}/libpng-1.2.56" ] && ln -fs ${LINKPATH}/libpng-1.2.56 . || echo "${LINKPATH}/libpng-1.2.56 not exist"
else
  [ -d "${LINKPATH}/libpng-1.2.50" ] && ln -fs ${LINKPATH}/libpng-1.2.50 . || echo "${LINKPATH}/libpng-1.2.50 not exist"
fi
[ -d "${LINKPATH}/zlib-1.2.8" ] && ln -fs ${LINKPATH}/zlib-1.2.8 . || echo "${LINKPATH}/zlib-1.2.8 not exist"

#link new ncep library to LIBPATH
cd ${LIBPATH} || exit 81
[ -d "${LINKPATH}/NCEPLIB" ] && ln -fs ${LINKPATH}/NCEPLIB . || echo "${LINKPATH}/NCEPLIB not exist"

#link netcdf library to LIBPATH
cd ${LIBPATH} || exit 81
[ -d "${LINKPATH}/netcdf-3.6.3" ] && ln -fs ${LINKPATH}/netcdf-3.6.3 . || echo "${LINKPATH}/netcdf-3.6.3 not exist"


#
#-------------------cwb mpmd-----------------------
#

cd ${PWDPATH}/src/cwb_mpmd.fd;
mkdir -p $LIBPATH/cwb_mpmd;
if [ ${at_MACHINE} = pcc ] ; then
  make -f Makefile_pcc
elif [ ${at_MACHINE} = fx10 ] ; then
  make -f Makefile_fx10
elif [ ${at_MACHINE} = fx100 ] ; then
  make -f Makefile_fx100
fi
#
#-------------------post processor-----------------
#

cd ${PWDPATH}/src/rsm_pgrb.fd;
if [ ${at_MACHINE} = pcc ] ; then
  make -f makefile_awips_ifort
elif [ ${at_MACHINE} = fx10 ] ; then
  make -f makefile_awips_fx10
elif [ ${at_MACHINE} = fx100 ] ; then
  make -f makefile_awips_fx100
fi

#
#--------------required data base------------------
#
# landuse, soiltype, vegetation fraction from WRF data base
mkdir -p ${PWDPATH}/fix/GEOG_V391
if [ ${at_MACHINE} = pcc ] ; then
  ln -fs /home/xb127/DATA/DATA_GEOG_V391/* ${PWDPATH}/fix/GEOG_V391/
else
  ln -fs /nwpr/gfs/xb127/data2/DATA/GEOG_V391/* ${PWDPATH}/fix/GEOG_V391/
fi
#
# 3drt data from Dr. Lee Wei-Liang
mkdir -p ${PWDPATH}/fix/Param_3DRT
if [ ${at_MACHINE} = pcc ] ; then
  ln -fs /home/xb127/DATA/PARA_3DRT/Param_3DRT/* ${PWDPATH}/fix/Param_3DRT
else
  ln -fs /nwpr/gfs/xb127/data2/DATA/PARA_3DRT/Param_3DRT/* ${PWDPATH}/fix/Param_3DRT
fi

#
#--------------base field data from GFS------------
#
mkdir -p ${PWDPATH}/wrk/DATA_GFS;
if [ ${at_MACHINE} = pcc ] ; then
  echo "GFS data at /home/xb127/DATA/DATA_GFS/synOP_ZC_21060200_initT511"
  ln -fs /home/xb127/DATA/DATA_GFS/synOP_ZC_21060200_initT511 ${PWDPATH}/wrk/DATA_GFS/
else
  echo "GFS data at /nwpr/gfs/xb127/data2/DATA/DATA_GFS/synOP_ZC_21060200_initT511"
  ln -fs /nwpr/gfs/xb127/data2/DATA/DATA_GFS/synOP_ZC_21060200_initT511 ${PWDPATH}/wrk/DATA_GFS/
fi

#
#-------------------model components---------------
#
cat > ${PWDPATH}/run/set_machine << EOF
export MACHINE=${MACHINE}
EOF

if [ ${do_testcase} = yes ]; then
  cd ${PWDPATH}/run;
  ./compile > compile.log 2>&1 &
  echo "check run/compile.log to see if compilation is finished or not"
else
  echo "No test case is set. Go to run/configure to setup your own case"
fi

