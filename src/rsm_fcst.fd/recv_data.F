      subroutine recv_data(root_gfs,itag)
!yj      subroutine recv_data(fhour,nx,my,lev,temp,spfh,clwr &
!yj     &                     ,ozon,geop,u,v           &
!yj     &                     ,tg,smc,snr,stc,cice)
!
!
      use module_gfsdata
      integer root_gfs,itag
      integer k
!yj: for new NoahLSM (NOAH MP), 4 soil layers are needed, but for OSU LSM only 2
!yj: soil layers are need. For convenient, GFS always gives RSM 4 soil
!yj: layer(kmsoil), but lsoil will be 4 for NOAHLSM and will be 2 for OSU.
!in module_gfsdata:      integer kmsoil
!in module_gfsdata:      parameter(kmsoil=4)

! 
!yj      if (myrank_rsm.eq.0) then
!yj        call allocate_gfsdata
        len2=clngrd
        len3=clngrd*levs

!for atmospheric fields        
        itag=itag+1
        call mpmd_recv(fhour,1,root_gfs,itag,'R')
        print*,'recv fhour: itag=',itag

        itag=itag+1
        call mpmd_recv(temp,len3,root_gfs,itag,'R')
        call maxmin(temp,len2,levs,1,levs,'recv_temp')

        itag=itag+1
        call mpmd_recv(spfh,len3,root_gfs,itag,'R')
        call maxmin(spfh,len2,levs,1,levs,'recv_spfh')

        itag=itag+1
        call mpmd_recv(clwr,len3,root_gfs,itag,'R')

!>> for WSM6 tracers
!       itag=itag+1
!       call mpmd_recv(rain,len3,root_gfs,itag,'R')
!
!       itag=itag+1
!       call mpmd_recv(qice,len3,root_gfs,itag,'R')
!
!       itag=itag+1
!       call mpmd_recv(snow,len3,root_gfs,itag,'R')
!
!       itag=itag+1
!       call mpmd_recv(grpl,len3,root_gfs,itag,'R')
!<<
        
        itag=itag+1
        call mpmd_recv(ozon,len3,root_gfs,itag,'R')
        call maxmin(ozon,len2,levs,1,levs,'recv_ozon')
        
        itag=itag+1
        call mpmd_recv(geop,len2,root_gfs,itag,'R')
        print*,'recv_geop 1'
        call maxmin(geop,len2,1,1,1,'recv_geop')
!yj2019        
        itag=itag+1
        call mpmd_recv(terr,len2,root_gfs,itag,'R')
        print*,'recv_terr 1'
        call maxmin(terr,len2,1,1,1,'recv_terr')

        itag=itag+1
        call mpmd_recv(u,len3,root_gfs,itag,'R')
        call maxmin(u,len2,levs,1,levs,'recv_u')
        
        itag=itag+1
        call mpmd_recv(v,len3,root_gfs,itag,'R')
        call maxmin(v,len2,levs,1,levs,'recv_v')

        print*,'recv temp,spfh,clwr,rain,qice,snow,grpl,ozon,geop,terr,u,v'

!for surface fields       
        itag=itag+1
        call mpmd_recv(tg,len2,root_gfs,itag,'R')
        print*,'recv_tg 1'
        call maxmin(tg,len2,1,1,1,'recv_tg')
        
        itag=itag+1
        call mpmd_recv(smc,len2*kmsoil,root_gfs,itag,'R')
        call maxmin(smc,len2,kmsoil,1,kmsoil,'recv_smc')

        itag=itag+1
        call mpmd_recv(snr,len2,root_gfs,itag,'R')
        where (snr<1.e-3) snr=0.0
        print*,'recv_snr 1'
        call maxmin(snr,len2,1,1,1,'recv_snr')

        itag=itag+1
        call mpmd_recv(stc,len2*kmsoil,root_gfs,itag,'R')
        call maxmin(stc,len2,kmsoil,1,kmsoil,'recv_stc')

        itag=itag+1
        call mpmd_recv(cice,len2,root_gfs,itag,'R')
        print*,'recv_cice 1'
        call maxmin(cice,len2,1,1,1,'recv_cice')
!yj2019
        itag=itag+1
        call mpmd_recv(slmsk,len2,root_gfs,itag,'R')
        print*,'recv_slmsk 1'
        call maxmin(slmsk,len2,1,1,1,'recv_slmks')

        print*,'recv tg,smc,snr,stc,cice,slmsk: itag=',itag
        
!yj      endif  
!
      return
      end
