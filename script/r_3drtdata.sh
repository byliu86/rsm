#! /bin/sh
#
set -ex
#
PROG=r3drtdata
WORKDIR=$1
MACHINE=$2
WORK=$3
#
#     namelist /naminfo/ TS_LAT, TE_LAT, TS_LON, TE_LON
#
echo " &naminfo" > info_3drt.parm
echo " TS_LAT=18.0," >> info_3drt.parm
echo " TE_LAT=29.0," >> info_3drt.parm
echo " TS_LON=115.0," >> info_3drt.parm
echo " TE_LON=126.0," >> info_3drt.parm
echo " condir='$WORKDIR'," >> info_3drt.parm
echo " FILE_NAME='nc3.nc'," >> info_3drt.parm
echo " &END" >> info_3drt.parm

# get the land sea mask of the RSM/MSM domain from "RMTN"
if [ $MACHINE = Fujitsu_fx10 ]||[ $MACHINE = Fujitsu_fx100 ] ; then
ln -fs ${WORK}/rmtn_init/rmtnslm         fort.23
else
export FORT23=${WORK}/rmtn_init/rmtnslm
fi

#
ln -fs  ${FIXDIR}/Param_3DRT/* .
cat rsmlocationinit info_3drt.parm > r3drtdata.parm
#
ls -l
pwd
##
ln -fs $EXPEXE/${PROG}.x $PROG.x
./$PROG.x <r3drtdata.parm >stdout.r3drtdata  || exit
cat stdout.r3drtdata
rm -f fort.[0-9]* 2>/dev/null

################  end of file  ##################

#>>----3DRT data information----->>      
